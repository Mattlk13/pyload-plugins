pyLoad-Plugins
==============

My own pyLoad plugins/addons:

* **NotifyGNTP** *(Send Notifications to Growl or Snarl)*<br/>
  This plugin sends notifications (captcha waiting, download finish, etc.) to
  Growl or Snarl via socket based [GNT protocol](http://www.growlforwindows.com/gfw/help/gntp.aspx).


Installation
------------

Just copy the python files into their appropriate folder into `userplugins/{crypter|hooks|...}`.

NotifyGNTP requires python package [gntp](https://github.com/kfdm/gntp); simple download and install it or
use the automatic package management tool `pip` by typing

```shell
  pip install gntp
```
on a shell terminal.


Bugs
----

[GitHub issue tracker](https://github.com/joberreiter/pyload-plugins/issues)


Changelog
---------

[v0.18](https://github.com/joberreiter/pyload-plugins/compare/0.13-newhooklayout...v0.18-newhooklayout)
- New notification type 'pyLoad general'
- Support for SSL based callback urls
- Add license file

[v0.06](https://github.com/joberreiter/pyload-plugins/tree/ccc21e9c766daaa2f16289bd7b093add4445a68b)
- Initial public release


Donations
---------

[![Donation](https://img.shields.io/badge/donate-paypal-orange.svg)](https://www.paypal.com/cgi-bin/webscr?item_name=pyLoad+plugins&cmd=_donations&lc=US&currency_code=EUR&business=joberreiter%40gmx.net)
