# -*- coding: utf-8 -*-

import gntp.notifier    # Growl Network Transport Protocol Implementation
import socket           # for IP address determination
import os               # for loading of the pyLoad logo

from time import time
#from module.plugins.internal.Hook import Hook
from module.plugins.internal.Addon import Addon, Expose

class NotifyGNTP(Addon):
    __name__    = "NotifyGNTP"
    __type__    = "hook"
    __version__ = "0.21"
    __status__  = "testing"

    __description__ = """Send notifications to Growl & Snarl via GNTP"""
    __license__     = "GPLv3"
    __authors__     = [("Jochen Oberreiter", "joberreiter.AT.gmx.DOT.net")]

    __config__ = [("hostname"       , "str" , "Hostname"                            , "127.0.0.1"),
                  ("password"       , "str" , "Password"                            , ""),
                  ("port"           , "int" , "Port"                                , 23053),
                  ("notifycaptcha"  , "bool", "Notify captcha request"              , True),
                  ("notifypackage"  , "bool", "Notify package finished"             , True),
                  ("notifyprocessed", "bool", "Notify packages processed"           , True ),
                  ("notifyupdate"   , "bool", "Notify plugin updates"               , True ),
                  ("notifyexit"     , "bool", "Notify pyLoad shutdown"              , True ),
                  ("timeout"        , "int" , "Timeout between captchas in seconds" , 5),
                  ("sendpermin"     , "int" , "Max notifications per minute"        , 12 ),
                  ("displaysticky"  , "bool", "Display notifications sticky"        , False),
                  ("displaypriority", "int" , "Display priority, range -2(very low) to 2(emergency)" , -1),
                  ("force"          , "bool", "Send notifications even if client is connected", True),
                  ("logo"           , "str" , "Url or path to logo image"           , "/usr/share/pyload/icons/logo.png")]

    NOTIFICATION_TYPES = ["pyLoad general", "Captcha waiting", "Package finished", "Downloads finished"]


    def init(self):
        self.event_map  = {"allDownloadsProcessed": "all_downloads_processed",
                           "pluginUpdated": "plugin_updated"}
        self.last_notify = 0
        self.notifications = 0

    def activate(self):
        # Get logo uri
        self.logosource = self.get_config('logo')

        # Determine callback URI
        if self.pyload.config['webinterface']['activated']:
            self.webip = socket.gethostbyname(socket.gethostname())
            self.webport = self.pyload.config['webinterface']['port']
            if self.pyload.config['webinterface']['https']:
                self.callbackuri = "https://%s:%d" % (self.webip, self.webport)
            else:
                self.callbackuri = "http://%s:%d" % (self.webip, self.webport)
            self.log_debug("Set callback URI to [%s]" % self.callbackuri)

        # Register at remote host
        self.notifier_handle = self.register()
        if not self.notifier_handle:
            self.fail("Could not register at host '%s:%d'" % (self.get_config('hostname'), self.get_config('port')))
        else:
            self.log_info("Registered at host '%s:%d'" % (self.get_config('hostname'), self.get_config('port')))


    def plugin_updated(self, type_plugins):
        if not self.get_config('notifyupdate'):
            return

        self.notify(_("Plugins updated"), str(type_plugins), "pyLoad general")


    def captcha_task(self, task):
        if not self.get_config("notifycaptcha"):
            return

        if self.pyload.config['webinterface']['activated']:
            self.notify(_("Captcha"), _("New request waiting user input"), "Captcha waiting", self.callbackuri)
        else:
            self.notify(_("Captcha"), _("New request waiting user input"), "Captcha waiting")


    def package_finished(self, pypack):
        if self.get_config('notifypackage'):
            self.notify(_("Package finished"), pypack.name, "Package finished")


    def all_downloads_processed(self):
        if not self.get_config('notifyprocessed'):
            return

        # Check queue on package status
        if any(True for pdata in self.core.api.getQueue() if pdata.linksdone < pdata.linkstotal):
            self.notify(_("Package failed"), _("One or more packages were not completed successfully"), "Downloads finished")
        else:
            self.notify(_("All packages finished"), "", "Downloads finished")


    def exit(self):
        if not self.get_config('notifyexit'):
            return

        if self.pyload.do_restart:
            self.notify(_("Restarting pyLoad"), "", "pyLoad general")
        else:
            self.notify(_("Exiting pyLoad"), "", "pyLoad general")


    def load_logo(self, uri):
        if uri.startswith('http'):
            image = uri
        else:
            try:
                image = open(uri, 'rb').read()
            except:
                image = None
                self.log_info("Application logo not found at '%s', skipping it" % uri)
        return image


    def register(self):
        notifier = gntp.notifier.GrowlNotifier(applicationName = "pyLoad",
                                               notifications = self.NOTIFICATION_TYPES,
                                               defaultNotifications = self.NOTIFICATION_TYPES,
                                               applicationIcon = self.load_logo(self.logosource),
                                               hostname = self.get_config("hostname"),
                                               password = self.get_config("password"),
                                               port = self.get_config("port"))
        if notifier:
            notifier.register()
        return notifier

    @Expose
    def notify(self, note_title, note_msg = "", note_type = "None", note_callbackuri = None):
        if self.pyload.isClientConnected() and not self.get_config('force'):
            return

        # Check notification type
        if note_type not in self.NOTIFICATION_TYPES:
            self.log_warning("Unknown notification type '%s', using default '%s'" % (str(note_type), str(self.NOTIFICATION_TYPES[0])))
            note_type = self.NOTIFICATION_TYPES[0]

        # Check timeouts
        elapsed_time = time() - self.last_notify;
        if (elapsed_time) < self.get_config('timeout'):
            return
        if elapsed_time > 60:
            self.notifications = 0
        elif self.notifications >= self.get_config('sendpermin'):
            return

        # Check priority
        note_prio = self.get_config('displaypriority')
        if not (-2 <= note_prio <= 2):
            note_prio = -1
            self.log_warning("Priority out of range, check plugin config. Using default value [-1] instead")

        # Encode title and message if necessary
        if isinstance(note_title, unicode):
            note_title = note_title.encode('utf8')
        if isinstance(note_msg, unicode):
            note_msg = note_msg.encode('utf8')

        # Send notification
        if not self.notifier_handle:
            self.log_warning("No handle to notifier object available, notification [%s] not sent." % note_title)
        else:
            self.notifier_handle.notify(noteType = note_type,
                                        title = note_title,
                                        description = note_msg,
                                        callback = note_callbackuri,
                                        icon = self.load_logo(self.logosource),
                                        sticky = self.get_config('displaysticky'),
                                        priority = note_prio)
        self.last_notify = time()
        self.notifications +=1

        return True
